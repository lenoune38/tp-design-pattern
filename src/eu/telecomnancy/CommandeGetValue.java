package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeGetValue implements Commande{

	private ISensor sensor;
	
	public CommandeGetValue(ISensor sensor) {
		this.sensor=sensor;
	}

	public double execute() throws SensorNotActivatedException{
		return this.sensor.getValue();
	}

}

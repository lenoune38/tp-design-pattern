package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOff implements Commande{

	private ISensor sensor;
	
	public CommandeOff(ISensor sensor) {
		this.sensor=sensor;
	}

	public void execute(){
		this.sensor.off();
	}

}

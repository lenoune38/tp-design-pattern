package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOn implements Commande{

	private ISensor sensor;

	public CommandeOn(ISensor sensor) {
		this.sensor=sensor;
	}
	
	public void execute(){
		this.sensor.on();
	}

}

package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate implements Commande{

	private ISensor sensor;

	public CommandeUpdate(ISensor sensor) {
		this.sensor=sensor;
	}

	public void execute() throws SensorNotActivatedException{
		this.sensor.update();
	}
}

package eu.telecomnancy.sensor;

public class Adapteur implements ISensor{

	LegacyTemperatureSensor sensor;

	public Adapteur(LegacyTemperatureSensor legTempSens) {
		this.sensor = legTempSens;
	}

	public void on() {
		if (!sensor.getStatus()){
			sensor.onOff();
		}
	}

	public void off() {
		if (sensor.getStatus()){
			sensor.onOff();
		}
	}

	public boolean getStatus() {
		return sensor.getStatus();
	}

	public void update() throws SensorNotActivatedException {

	}


	public double getValue() throws SensorNotActivatedException {
		return sensor.getTemperature();
	}

}
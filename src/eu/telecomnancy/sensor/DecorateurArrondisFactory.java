package eu.telecomnancy.sensor;

public class DecorateurArrondisFactory extends SensorFactory {

	public ISensor getSensor() {
		return new DecorateurArrondis(new TemperatureSensor());
	}

	
}

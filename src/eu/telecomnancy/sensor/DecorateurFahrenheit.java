package eu.telecomnancy.sensor;

import java.util.Observable;

public class DecorateurFahrenheit extends Observable implements ISensor {

	private ISensor sensor;
	public DecorateurFahrenheit(ISensor s) {
		this.sensor=s;
	}

	@Override
	public void on() {
		this.sensor.on();
		setChanged();
		notifyObservers();
	}

	@Override
	public void off() {
		this.sensor.off();
		setChanged();
		notifyObservers();
	}

	@Override
	public boolean getStatus() {
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.sensor.update();
		setChanged();
		notifyObservers();
	}

	public double getValue() throws SensorNotActivatedException {
		return 1.8*this.sensor.getValue()+32;
	}

}

package eu.telecomnancy.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */

public class SensorProxy implements ISensor {
    private ISensor sensor;
    private SensorLogger log;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private Date date = new Date();
    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = _sensor;
        log = sensorLogger;
    }

    @Override
    public void on() {
        log.log(LogLevel.INFO, dateFormat.format(date)+" : Sensor On");
        sensor.on();
    }

    @Override
    public void off() {
        log.log(LogLevel.INFO, dateFormat.format(date)+" Sensor Off");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        log.log(LogLevel.INFO, dateFormat.format(date)+" Sensor getStatus");
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, dateFormat.format(date)+" Sensor update");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, dateFormat.format(date)+" Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }
}

package eu.telecomnancy.statesensor;

import java.util.Observable;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.statesensor.StateSensorOff;
import eu.telecomnancy.statesensor.StateSensor;
import eu.telecomnancy.statesensor.StateSensorOn;

public class StateSensor extends Observable implements ISensor {
	ISensor state = new StateSensorOff();
	
    @Override
    public void on() {
        state = new StateSensorOn();
        setChanged();
        notifyObservers();
    }

    @Override
    public void off() {
        state = new StateSensorOff();
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
       this.state.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }

}
package eu.telecomnancy.statesensor;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateSensorOff implements ISensor {

	public StateSensorOff() {

	}

	public static void main(String[] args) {

	}

	@Override
	public void on() {
		
	}

	@Override
	public void off() {

		
	}

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}

package eu.telecomnancy.statesensor;

import java.util.Random;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateSensorOn implements ISensor {
	double value = 0;
	
	public StateSensorOn() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getStatus() {
		return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}
}
package eu.telecomnancy.ui;

import eu.telecomnancy.CommandeGetValue;
import eu.telecomnancy.CommandeOff;
import eu.telecomnancy.CommandeOn;
import eu.telecomnancy.CommandeUpdate;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class SensorView extends JPanel implements Observer {
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
        ((Observable) this.sensor).addObserver(this);
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	CommandeOn cmd= new CommandeOn(sensor);
                cmd.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	CommandeOff cmd= new CommandeOff(sensor);
                cmd.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                	CommandeUpdate cmd= new CommandeUpdate(sensor);
                    cmd.execute();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	public void update(Observable o, Object arg) {
		this.sensor = (ISensor) o;
		try {
			if(this.sensor.getStatus()){
				CommandeGetValue cmd = new CommandeGetValue(sensor);
				this.value.setText(cmd.execute()+"");
			}else{
				this.value.setText("N/A °C");
			}
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
